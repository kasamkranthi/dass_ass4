import os
from colorama import init, Fore,Back

###############################################################
import logging
# logging to debug via terminal
# tail -f your.log
def part():
    logging.debug("----------------------------------------")


logging.basicConfig(filename='test.log',
                    level=logging.DEBUG,
                    filemode='w',
                    format='%(levelname)s:%(message)s')
##############################################################



# method in initialize.py of colorama library
init()

# method called in Game.py
def start():

    ###################################
    # Dinosaur on the start screen
    f=open("./utilities/dinasaur","r")
    dino=[]
    for i in f:
        dino.append(i)
    
    logging.debug(f"DIno is {dino}")
    logging.debug(f"DIno is {dino[3]}")
    logging.debug(f"type DIno is {type(dino)}")
    logging.debug(f"Length DIno is {len(dino)}")
    f.close()
    ####################################
    # Jurassic Jet text on starting the screen
    f=open("./utilities/banner","r")
    banner=[]
    for i in f:
        banner.append(i)
    f.close
    ##################################

    columns,rows=os.get_terminal_size()
    logging.info(f"rows are {rows} and cols are {columns}")
    matrix=[]    
    for i in range (rows): #rows                              
        new = []                 
        for j in range (columns): #columns  
            new.append(Back.BLACK+" "+'\x1b[0m')      
        matrix.append(new)

    ##################################################
    # Draws dinosaur
    for i in range(29):
        for j in range(75):
            #[BAD]
            matrix[i+5][j+65]=Back.BLACK+dino[i][j]+'\x1b[0m'

    # Drawing RED banner
    for i in range(11):
        for j in range(48):
            #[BAD]
            matrix[i+5][j+10]=Back.BLACK+Fore.RED+banner[i][j]+'\x1b[0m'
    ###################################################
    
    data=""
    data+="W:GOING UP ,A:LEFT ,D:RIGHT ,SPACE:SHOOT ,P:SHIELD ,Q:QUIT ,T:TRANSFORM ,,,PRESS 'ENTER' TO CONTINUE"

    j=0
    x=0
    for i in data:
        if i==",":
            x+=2
            j=0
        else:
            #[BAD]
            matrix[20+x][j+12]=Back.BLACK+Fore.WHITE+i+'\x1b[0m'
            j+=1

    for i in range(rows):
        for j in range(columns):
            print(matrix[i][j],end="")
        print()

    # Waits for input character
    inp=input()